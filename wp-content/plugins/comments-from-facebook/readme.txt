=== Facebook comments WordPress ===
Contributors: wpdevart
Tags: facebook comments, facebook comment, facebook comment notification, seo facebook comments, facebook comments import, facebook comment system, comments, comment, wordpress comment, wordpress comments, Facebook, facebook badge, facebook connect, facebook group, facebook integration, Facebook like widget, facebook meta, facebook meta tag, Facebook Open Graph, Facebook Page, facebook platform, facebook plugin, post to facebook, Facebook feed, facebook wall for wordpress, Facebook Stream, Facebook Widget,
Requires at least: 2.9
Tested up to: 4.3
Stable tag: 1.0.1

Our WordPress Facebook comments plugin will help you to display Facebook Comments box on your website. You can use Facebook Comments box on your pages/posts.

== Description ==

Facebook comments plugin is great tool that will allow you to show your visitors Facebook comments on your website. At the same time this plugin is very useful for improving your website traffic from Facebook. Facebook Comments box  is important part of social optimization as  well, that's why most of websites use Facebook Comments box on their websites.
WordPress Facebook comments plugin is very easy to use, you just need to create Facebook App ID and use it on your website.
You can disable Facebook Comments on any page/post you need.

### View our Facebook comments plugin Demo page: 

[Facebook comments Demo](http://demo.wpdevart.com/facebook-comments-box/)

### Features of WordPress Facebook comments:

* **User friendly and easy to use.**
* **Tested with other popular plugins.**
* **Ability to import Facebook Comments box to your WordPress website.**
* **Ability to type Facebook Comments box Title.**
* **Ability to type Facebook Comments box Title text color.**
* **Ability to type Facebook Comments box Title font-size.**
* **Ability to type Facebook Comments box Title font family.**
* **Ability to select Comments box Title position.**
* **Ability to select where to display Facebook Comments box.**
* **Ability to set Facebook Comments box width.**
* **Ability to set number of comments of Facebook Comments box.**
* **Ability to set Facebook Comments box language.**

You can upgrade WordPress Facebook Comments plugin to [Facebook Comments Pro](http://wpdevart.com/wordpress-facebook-comments-plugin) to add more features.

### Features of WordPress Facebook comments Pro:

* **Ability to set WordPress Facebook Comments box color scheme.**
* **Ability to set WordPress Facebook Comments box Background color.**
* **Ability to set WordPress Facebook Comments box position.**
* **39 awesome animation effects for WordPress Facebook Comments box.**
* **Premium Support.**

### If you found any bug in our WordPress Facebook comments plugin or have a question contact us at support@wpdevart.com.

Dear users, we also recommend you to take a look for this useful plugins - [WordPress Coming Soon](https://wordpress.org/plugins/coming-soon-page/), [WordPress Poll plugin](https://wordpress.org/plugins/polls-widget/), [WordPress Countdown plugin](https://wordpress.org/plugins/widget-countdown), [WordPress YouTube](https://wordpress.org/plugins/youtube-video-player/), [WordPress Facebook like box](https://wordpress.org/plugins/like-box/).

== Installation ==

### It's very simple:

1. Download WordPress Facebook Comments plugin.   
2. Upload the Facebook Comments plugin from your admin panel.    
3. Then activate the Facebook Comments plugin. 
4. After that type the Facebook App ID.    
5. Then configure the plugin and add Facebook comments box.

Dear users, we also recommend you to take a look for this useful plugins - [WordPress Coming Soon](https://wordpress.org/plugins/coming-soon-page/), [WordPress Poll plugin](https://wordpress.org/plugins/polls-widget/), [WordPress Countdown plugin](https://wordpress.org/plugins/widget-countdown), [WordPress YouTube](https://wordpress.org/plugins/youtube-video-player/), [WordPress Facebook like box](https://wordpress.org/plugins/like-box/). 

== Frequently Asked Questions ==

Facebook Comments plugin is an user friendly plugin, but on this page you can find some frequently asked questions that will help you.

= Where I need to find App ID =

APP ID - you can create your App Id on this page - https://developers.facebook.com/apps.
Also, here is another tutorial of creating App Id, you can check it - https://help.yahoo.com/kb/SLN18861.html.

= How can I use WordPress Facebook comments plugin shortcode =

Here's an example of using the shortcode in posts, pages:

`[wpdevart_facebook_comment facebook_app_id="1638418549774901" curent_url="http://developers.facebook.com/docs/plugins/comments/" title_text="Facebook Comment" title_text_color="#000000" title_text_font_size="22" title_text_font_famely="monospace" title_text_position="left" width="100%" bg_color="#CCCCCC" animation_effect="random" locale="en_US" count_of_comments="2" ]`

Here's an example of using the shortcode in PHP code:

`<?php echo do_shortcode('[wpdevart_facebook_comment curent_url="http://developers.facebook.com/docs/plugins/comments/" title_text="Facebook Comment" title_text_color="#000000" title_text_font_size="22" title_text_font_famely="monospace" title_text_position="left" width="100%" bg_color="#CCCCCC" animation_effect="random" count_of_comments="2" ]'); ?>`

Dear users, we also recommend you to take a look for this useful plugins - [WordPress Coming Soon](https://wordpress.org/plugins/coming-soon-page/), [WordPress Poll plugin](https://wordpress.org/plugins/polls-widget/), [WordPress Countdown plugin](https://wordpress.org/plugins/widget-countdown), [WordPress YouTube](https://wordpress.org/plugins/youtube-video-player/), [WordPress Facebook like box](https://wordpress.org/plugins/like-box/).

== Screenshots ==

1.  WordPress Facebook comments Front-end  
2.  WordPress Facebook comments Back-end
3.  WordPress Facebook comments Front-end
4.  WordPress Facebook comments Back-end 

== Changelog ==

= 1.0.0 =

*  Initial version of WordPress Facebook comments.

==Facebook comments WordPress step by step guide==

### Facebook Comments plugin main options

*   APP ID  - Type here your Facebook App ID
*   Title - Type here Facebook comments box title
*   Color scheme - Select Facebook comments box Color scheme
*   Title text color - Select Facebook comments box title text color
*   Title font-size - Type Facebook comments box title font-size
*   Title font family - Select Facebook comments box title font family
*   Title position - Select Facebook comments box title position
*   Display comment on - Select where to display Facebook comments
*   Comment box width - Type here the Facebook comments box width
*   Number of comments show - Type here the comments of Facebook comments to display 
*   Animation effect - Select the animation effect for Facebook comments box
*   Background color - Select Facebook comments background color
*   Facebook comments position -Select Facebook comments box position(before or after WordPress standard comments)
*   Facebook comments language - Type here Facebook comments language code(en_US,de_DE...) 

### Adding Facebook Comments plugin shortcode to pages, posts and in Php code

Here's an example of using the shortcode in posts, pages:
`[wpdevart_facebook_comment facebook_app_id="1638418549774901" curent_url="http://developers.facebook.com/docs/plugins/comments/" title_text="Facebook Comment" title_text_color="#000000" title_text_font_size="22" title_text_font_famely="monospace" title_text_position="left" width="100%" bg_color="#CCCCCC" animation_effect="random" locale="en_US" count_of_comments="2" ]`

Here's an example of using the shortcode in PHP code:
`<?php echo do_shortcode('[wpdevart_facebook_comment facebook_app_id="1638418549774901" curent_url="http://developers.facebook.com/docs/plugins/comments/" title_text="Facebook Comment" title_text_color="#000000" title_text_font_size="22" title_text_font_famely="monospace" title_text_position="left" width="100%" bg_color="#CCCCCC" animation_effect="random" locale="en_US" count_of_comments="2" ]'); ?>`

Here are explanation of Facebook comments shoetcode attributes.

curent_url - Type the URL of a page from where you need to show Facebook comments 
title_text - Type here Facebook comments box title
title_text_color - Select Facebook comments box title text color
title_text_font_size - Type Facebook comments box title font-size
title_text_font_famely - Select Facebook comments box title font family
title_text_position - Select Facebook comments box title position
width - Type here the Facebook comments box width
count_of_comments - Type here the comments of Facebook comments to display
bg_color - Select Facebook comments background color
animation_effect - Select the animation effect for Facebook comments box

Dear users, we also recommend you to take a look for this useful plugins - [WordPress Coming Soon](https://wordpress.org/plugins/coming-soon-page/), [WordPress Poll plugin](https://wordpress.org/plugins/polls-widget/), [WordPress Countdown plugin](https://wordpress.org/plugins/widget-countdown), [WordPress YouTube](https://wordpress.org/plugins/youtube-video-player/), [WordPress Facebook like box](https://wordpress.org/plugins/like-box/) .



